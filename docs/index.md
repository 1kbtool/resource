仅供学习交流使用，请支持正版！

快速预览：

综合性->阿里网盘搜索（支持在线观看，资源多，下载快）：https://www.alipansou.com/

电影、电视剧->豆瓣资源下载大师（自动匹配网络公开资源，不过下载较慢）：https://greasyfork.org/zh-CN/scripts/329484 （需安装油猴），RARBG：https://www.rarbgmirror.com/torrents.php

动漫相关->ACG.RIP：http://acg.rip/

游戏->fitgirl（比较纯净）：https://fitgirl-repacks.site/

音乐->洛雪助手（软件，手机、电脑均有，需要手动配置）：https://github.com/lyswhut/lx-music-desktop

电子书->Zlib（可能要梯子）：https://zh.z-lib.org/

轻小说->wenku8：https://www.wenku8.net/

字幕->射手网：https://assrt.net/

TG搜索：https://www.sssoou.com/

BT（torrent）文件下载工具（Windows）：https://www.fosshub.com/qBittorrent.html?dwl=qbittorrent_4.3.9_x64_setup.exe （请不要使用迅雷！）

