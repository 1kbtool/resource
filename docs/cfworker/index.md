---
title: "Start"
slug: "Start"
date: 2022-05-02T10:47:41+08:00
draft: false
---

【摘要】 使用cloud flare worker 服务搭建“镜像”页面以快捷访问某些网站而无需在本地配置代理
【关键词】 proxy；镜像；cloudflare；worker，GitHub，zlibrary；免费；代码

转到：[网络安全ddos防护_免费cdn加速_智能化云服务平台 | Cloudflare 中国官网 | Cloudflare](https://www.cloudflare.com/zh-cn/)

> 注意：Cloudflare是有官方中文的，在界面的右上角可以选择语言。

从官网注册之后，会跳转到转到 https://dash.cloudflare.com/，接下来的步骤都是在控制台进行的。

在控制面板主页左侧可以找到`workers`。如图所示，中间可以创建服务，右侧显示每天的额度，如果只是搭建个人服务这些额度绰绰有余了（但是放在网上公开使用大概率会用完，这也是我推荐大家自己搭建的原因）。下方会显示所有已经搭建的服务。

![image-20220502110006820](image-20220502110006820.png)

创建服务，随便填写一个即可，记住下面这个`https://xxxx.xxx.workers.dev`，此即**镜像站的网址。**

![注意服务名称](image-20211213225039559.png)

等待部署完成，回到 https://dash.cloudflare.com/，workers-点击刚刚创建的项目。转到控制界面，可以找到右下角有一个快速编辑的按钮。

![个人觉得这个设计十分反人类](image-20211213225206714.png)

打开这个窗口，把代码以下复制进去，保存部署，然后直接访问之前我们记下的链接就好了（也可以在控制台看到）。

> 这是镜像zlibrary的示例，你也可以修改成镜像GitHub。

![image-20220502111948254](image-20220502111948254.png)

```js
// 你要镜像的网站.
const upstream = 'zh.u1lib.org'

// 镜像网站的目录，比如你想镜像某个网站的二级目录则填写二级目录的目录名，镜像 google 用不到，默认即可.
const upstream_path = '/'

// 镜像站是否有手机访问专用网址，没有则填一样的.
const upstream_mobile = 'zh.u1lib.org'

// 屏蔽国家和地区.
const blocked_region = ['KP', 'SY', 'PK', 'CU']

// 屏蔽 IP 地址.
const blocked_ip_address = ['0.0.0.0', '127.0.0.1']

// 镜像站是否开启 HTTPS.
const https = true

// 文本替换.
const replace_dict = {
    '$upstream': '$custom_domain',
    '//zh.u1lib.org': ''
}

// 以下保持默认，不要动
addEventListener('fetch', event => {
    event.respondWith(fetchAndApply(event.request));
})

async function fetchAndApply(request) {

    const region = request.headers.get('cf-ipcountry').toUpperCase();
    const ip_address = request.headers.get('cf-connecting-ip');
    const user_agent = request.headers.get('user-agent');

    let response = null;
    let url = new URL(request.url);
    let url_hostname = url.hostname;

    if (https == true) {
        url.protocol = 'https:';
    } else {
        url.protocol = 'http:';
    }

    if (await device_status(user_agent)) {
        var upstream_domain = upstream;
    } else {
        var upstream_domain = upstream_mobile;
    }

    url.host = upstream_domain;
    if (url.pathname == '/') {
        url.pathname = upstream_path;
    } else {
        url.pathname = upstream_path + url.pathname;
    }

    if (blocked_region.includes(region)) {
        response = new Response('Access denied: WorkersProxy is not available in your region yet.', {
            status: 403
        });
    } else if (blocked_ip_address.includes(ip_address)) {
        response = new Response('Access denied: Your IP address is blocked by WorkersProxy.', {
            status: 403
        });
    } else {
        let method = request.method;
        let request_headers = request.headers;
        let new_request_headers = new Headers(request_headers);

        new_request_headers.set('Host', url.hostname);
        new_request_headers.set('Referer', url.hostname);

        let original_response = await fetch(url.href, {
            method: method,
            headers: new_request_headers
        })

        let original_response_clone = original_response.clone();
        let original_text = null;
        let response_headers = original_response.headers;
        let new_response_headers = new Headers(response_headers);
        let status = original_response.status;

        new_response_headers.set('access-control-allow-origin', '*');
        new_response_headers.set('access-control-allow-credentials', true);
        new_response_headers.delete('content-security-policy');
        new_response_headers.delete('content-security-policy-report-only');
        new_response_headers.delete('clear-site-data');

        const content_type = new_response_headers.get('content-type');
        if (content_type.includes('text/html') && content_type.includes('UTF-8')) {
            original_text = await replace_response_text(original_response_clone, upstream_domain, url_hostname);
        } else {
            original_text = original_response_clone.body
        }

        response = new Response(original_text, {
            status,
            headers: new_response_headers
        })
    }
    return response;
}

async function replace_response_text(response, upstream_domain, host_name) {
    let text = await response.text()

    var i, j;
    for (i in replace_dict) {
        j = replace_dict[i]
        if (i == '$upstream') {
            i = upstream_domain
        } else if (i == '$custom_domain') {
            i = host_name
        }

        if (j == '$upstream') {
            j = upstream_domain
        } else if (j == '$custom_domain') {
            j = host_name
        }

        let re = new RegExp(i, 'g')
        text = text.replace(re, j);
    }
    return text;
}


async function device_status(user_agent_info) {
    var agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
    var flag = true;
    for (var v = 0; v < agents.length; v++) {
        if (user_agent_info.indexOf(agents[v]) > 0) {
            flag = false;
            break;
        }
    }
    return flag;
}
```
