## 简介

洛雪音乐助手，全网音乐聚合搜索、在线试听、下载，支持桌面歌词，不受限制。

适合台式机等联网设备使用。

电脑版 [项目地址](https://github.com/lyswhut/lx-music-desktop) [下载地址](https://kermgithub.kermshare.workers.dev/lyswhut/lx-music-desktop/releases)

>推荐下载lx-music-desktop-vXXX-win_x64-green.7z文件

安卓版 [项目地址](https://github.com/lyswhut/lx-music-mobile) [下载地址](https://kermgithub.kermshare.workers.dev/lyswhut/lx-music-mobile/releases)

>推荐下载lx-music-mobile-vXXX-arm64-v8a.apk文件

演示：

![](2022-01-14-11-42-18.png)

## 使用说明

如前所述，本软件只是一个简单的客户端，最关键的还是数据源的问题。不同的数据源接口，支持的功能是完全不一样的。

安装软件之后，默认是采用临时接口，**临时接口只能搜索，不能试听，更不能下载**

请按照下图所示将接口转换为测试接口，以便进行试听

![](2022-01-14-11-13-54.png)

下载功能默认关闭，需要手动开启。

![](2022-01-14-11-21-24.png)

通过添加其他接口，可以支持高音质下载。

![](2022-01-14-11-45-35.png)

之前我一直使用某位大佬开发的接口，不过最近大佬博客似乎被打了一波，然后接口也开始设限制了，哎，且用且珍惜吧。

[原文连接](https://www.6yit.com/8498.html) 推荐直接访问原文，因为这里的方法说不定用着用着就失效了。

[下载地址](https://soso.lanzouj.com/b00p9c94f#8mno) 密码8mno

使用说明：

![](2022-01-14-11-21-12.png)

![](2022-01-14-11-21-19.png)

>封禁规则：**接口请求频率限制，3秒内超过5次请求就会封禁IP**  
请务必注意，使用批量下载可能会下着下着就全部下载失败了，这是因为IP被封

参考：

1. https://www.6yit.com/8498.html
2. https://github.com/lyswhut/lx-music-desktop
