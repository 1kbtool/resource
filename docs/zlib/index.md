## Zlibrary

Z-Library是成立于2009年的网站，免费提供书籍和文章下载，Z-Library自称是全球最大的数字图书馆。

官网：[Z-Library。世界上最大的数字图书馆。](https://zh.z-lib.org/)

如不能访问可以尝试博主搭建的镜像网站，[链接](https://zlib.kerm8380.workers.dev/)（有访问次数限制）。

未注册每日限制下载5次，注册并验证邮箱后每日限制下载10次（只需要邮箱就可以注册），当然你也可以通过捐赠网站的方式获取更多下载次数和附加功能。

## Lore Free

链接https://lorefree.com/

大量mobi、epub格式书籍。未登录状态下，每天有3个下载额度（包括电子书和论文），登录EOS账号以后再增加2个下载额度。

最近访问有些慢？

![img](2022-02-27-13-37-25.png)

<!-- ## 书格

[链接](https://new.shuge.org/)

中国数字古籍图书馆，各种古籍文献书法地图都有。

![img](https://soft.kermsite.com/p/%E7%94%B5%E5%AD%90%E4%B9%A6%E4%B8%8B%E8%BD%BD%E7%BD%91%E7%AB%99%E5%90%88%E8%BE%91%E6%8E%A8%E8%8D%90%E4%BD%BF%E7%94%A8z-lib%E5%92%8Clorefree/2022-02-27-13-33-54.png) -->

## 鸠摩搜索

[链接](https://www.jiumodiary.com/)

属于网盘集合型，可找大学教材电子版。



## Z-lib 提示受限版本

此网站对中国访问限制，请使用代理工具。或者使用以上提到的镜像网站。或访问参考链接2以获得其他解决方案。

参考：

1. https://pangniao.net/z-library.html
2. https://pangniao.net/z-library-shouxian.html
